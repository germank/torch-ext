--- A net_proxy_observer listen to events
-- in the local system and broadcast over the net
-- to connected clients
module(..., package.seeall)
require 'observer'
local socket = require('socket')
--WARNING: modified http.lua to include a line PROXY=nil
local http = require('socket.http')

function urlencode(str)
   if (str) then
      str = string.gsub (str, "\n", "\r\n")
      str = string.gsub (str, "([^%w ])",
         function (c) return string.format ("%%%02X", string.byte(c)) end)
      str = string.gsub (str, " ", "+")
   end
   return str    
end



function tcp_observer(host, port)
    local t = {}
    -- Create a new TCP object
    local conn = assert(socket.tcp())
    -- Resolve host
    local ip = assert(socket.dns.toip(host), "Couldn't resolve host "..host)
    --Atempt to connecto to given host and port
    assert(conn:connect(ip, port))
    t.conn = conn

    function t:on_event(method_id, params)
        data = table.concat {"method_id = ",method_id,"\n"}
        data = table.concat {data, table.concat(params),"\n"}
        self.conn:send(data)
    end
    return t
end

function http_observer(host, port)
    local t = {}

    t.url = "http://"..host..":"..port
    
    function t:on_event(method_id, params)
        local keyvalues={}
        for k,v in pairs(params) do
            v = urlencode(v)
            table.insert(keyvalues, table.concat({k,v},"="))
        end
        local qs = table.concat {self.url,"/",method_id, "/?", table.concat(keyvalues,"&")}
        local url = table.concat {self.url,"/",method_id, "/"}
        local body = table.concat(keyvalues,"&")
        print(qs:sub(1,75)..(qs:len() <=75 and "" or  "..."))
        --local b, c, h = http.request({url=qs})--, proxy='http://proxy.unitn.it:3128'})
        local b, c, h = http.request(url, body)--, proxy='http://proxy.unitn.it:3128'})
        if not b and c ~= 200 then print ("http error: "..c) end
    end
    
    return t
end
