#!/usr/bin/env luajit
require 'torch'
require 'stats'

tests = {}

tester = torch.Tester()


function tests.TestSimilarity()
    local v1 = torch.Tensor {{1,0,3},{2,1,3},{1,1,1}}
    local v2 = torch.Tensor {{0,1,2},{3,1,3},{1,1,1}}
    local sim = stats.get_sim(v1, v2)


    tester:assertTensorEq(sim, torch.Tensor {0.8485, 0.9810, 1.0}, 1e-4)

    local v1 = torch.Tensor {1,2,3}
    local v2 = torch.Tensor {4,5,6}
    local sim1 = stats.get_sim(v1, v2)
    local v1 = torch.Tensor {{1,2,3}}
    local v2 = torch.Tensor {{4,5,6}}
    local sim2 = stats.get_sim(v1, v2)
    tester:asserteq(sim1[{1,1}], sim2[{1,1}])

    local v1 = torch.Tensor {1,2,3}
    local v2 = torch.Tensor {{1,2,3},{4,5,6}}
    local v3 = torch.Tensor {4,5,6}
    local sim1 = stats.get_sim(v1, v2)
    local sim2 = stats.get_sim(v1, v3)
    tester:asserteq(sim1[{1,1}], 1)
    tester:asserteq(sim1[{2,1}], sim2[{1,1}])
end

function tests.TestPearson()
    local v1 = torch.Tensor {1,0,3,2,1,3,1,1,1}
    local v2 = torch.Tensor {0,1,2,3,1,3,1,1,1}
    local r = stats.pearson(v1,v2)
    tester:assertTensorEq (torch.Tensor {r}, torch.Tensor{0.75675}, 1e-4)

    v1 = v1:reshape(1,v1:size()[1])
    v2 = v2:reshape(1,v2:size()[1])
    local r2 = stats.pearson(v1, v2)
    tester:asserteq(r, r2)


end
    

tester:add(tests)
tester:run()

