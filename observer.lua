module(..., package.seeall)

-- Register
local function register( self, observer, method )
	local t = {}
	t.o = observer
	if type(method) == 'string' then
		t.m = observer[method]
	else
		t.m = method
	end
	table.insert( self, t )
end

-- Deregister
local function deregister( self, observer, method )
	local i
	local n = #self
	for i = n, 1, -1 do
		if (not observer or self[i].o == observer) and
			 (not method   or self[i].m == method)
		then
			table.remove( self, i )
		end
	end
end

-- Notify
local function notify( self, ... )
	local i
	local n = #self
	for i = 1, n do
		self[i].m( self[i].o, ... )
	end
end

-- signal metatable
local mt = {
	__call = function( self, ... )
		self:notify(...)
	end
}

function signal()
	local t = {}
	t.register = register
	t.deregister = deregister
	t.notify = notify
	setmetatable( t, mt )
	return t
end

function dropping_signal(k, measure)
    local s = signal()
    local notify = s.notify
    measure = measure or 'epochs'

    local new_call_pass = nil
    if measure == 'epochs' then
        new_call_pass = function(x, k)
            local ret = not x or x % k == 0
            local nv  = x and x + 1 or 1
            return ret, nv
        end
    elseif measure == 'secs' then
        require 'os'
        new_call_pass = function(x, k)
            local ct = os.time()
            local ret = not x or (ct - x >= k)
            return ret, (ret and ct or x)
        end
    end

    local count_calls 
    s.notify = function(...)
        local should_call, next_value = new_call_pass(count_calls, k)
        count_calls = next_value
        if should_call then
            return notify(...)
        end
    end
    return s
end


--- Forwards all the signals "s" in o1 to
-- a slot in o2 called "on_s"
function forward(o1, o2)
    for fname, f in pairs(o1) do
        --check if it is a signal
        if getmetatable(f) == mt then
            local slot_fname = "on_"..fname
            if not pcall(function() 
            local slot = o2[slot_fname]
                f:register(o2, slot)
            end)
            then
                io.stderr:write('observer.forward: no slot '..slot_fname..' on destination\n')
            end
        end
    end
end

--- Forwards all the signals "s" in o1 to
-- a slot in o2 called "on_s" that, fires the same signal again
-- Also creates a signal that get fired automatically
-- on o2
function repeater(o1, o2)
    for fname, f in pairs(o1) do
        --check if it is a signal
        if getmetatable(f) == mt then
            o2[fname] = observer.signal()
            local slot_fname = "on_"..fname
            o2[slot_fname] =function(self,...)
                self[fname](...)
            end
            f:register(o2, o2[slot_fname])
        end
    end
end

function dropping_repeater(o, k, measure)
    r = {}
    for fname, f in pairs(o) do
        --check if it is a signal
        if getmetatable(f) == mt then
            r[fname] = dropping_signal(k, measure)
            local slot_fname = "on_"..fname

            r[slot_fname] = function (o,...)
                --Call the equivalent dropping signal
                r[fname](...)
            end
            --Register the new observer
            f:register(r, r[slot_fname])
        end
    end
    return r
end

-- Returns a proxy object for o2
-- that filters the slot calls
-- so only 1 every k passes through
function filtering_slot(o2, k, measure) 
    print 'WARNING: DEPRECATED'
    measure = measure or 'epochs'
    local t = {}
    -- Whenever an event notification arrives, 
    -- forward it to o2 once every k times
    local count_calls = {}
    local mt = {}

    local init_last_call = nil
    local new_call_pass = nil
    if measure == 'epochs' then
        init_last_call = function() return 0 end
        new_call_pass = function(x, k)
            return x % k == 0, x + 1
        end
    elseif measure == 'secs' then
        require 'os'
        init_last_call = function() return os.time() end
        new_call_pass = function(x, k)
            local ct = os.time()
            local ret = ct - x >= k
            return ret, (ret and ct or x)
        end
    end
        
    mt.__index = function(table, key)
        --TODO: also check that key starts with "on_"
        if o2[key] and type(o2[key]) == 'function' then
            return function(self,...)
                if not count_calls[key] then
                    count_calls[key]=init_last_call()
                end
                local should_call, next_value = new_call_pass(count_calls[key], k)
                count_calls[key] = next_value
                if should_call then
                    return o2[key](o2,...)
                end
            end
        elseif o2[key] then
            return o2[key]
        else
            return nil
        end
    end
    mt.__next = function(t, k)return  next(o2, k) end
    setmetatable(t, mt)

    return t
end

--[[Usage:
observer = require( "alt_obs" )

alice = {}
function alice:slot( param )
  print( self, param )
end

bob = {}
bob.alert = observer.signal()

bob.alert:register( alice, alice.slot )
bob.alert( "Hi there" )
bob.alert:deregister( alice )
bob.alert( "Hello?" )
--]]
