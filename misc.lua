function dir(t)
    local ks = {}
    for k,v in pairs(t) do
        table.insert(ks, k)
    end
    return table.concat(ks, " ")
end
